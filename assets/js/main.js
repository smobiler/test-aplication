/**
 * Report
 */
class Report {

    constructor() {
        this.filter_from = "";
        this.filter_to = "";
        this.reportItems = {};
        this.init();
    }

    /**
     * Init
     */
    init() {
        var self = this;

        $("#create_report_button").click(function () {
            self.reportRequest();
        });

        this.mainTable = $('#report_1').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true,
            "language": {
                "search": "Фильтр:",
                "info": "Показана страница _PAGE_ из _PAGES_",
                "emptyTable": "Данные отсутствуют в таблице",
                "paginate": {
                    "previous": "Предыдущий",
                    "next": "Следующий",
                }
            }
        });

        //Date range picker
        $('#reservation').daterangepicker(
            {
                ranges: {
                    'Сегодня': [moment(), moment()],
                    'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'За последние 7 дней': [moment().subtract(6, 'days'), moment()],
                    'За последние 30 дней': [moment().subtract(29, 'days'), moment()],
                    'За текущий месяц': [moment().startOf('month'), moment().endOf('month')],
                    'За предыдущий месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                locale: {
                    applyLabel: 'Применить',
                    cancelLabel: 'Отменить',
                    customRangeLabel: 'Выбрать'
                }
            },
            function (start, end) {
                self.filter_from = start.format('YYYY-MM-DD');
                self.filter_to = end.format('YYYY-MM-DD');
            }
        );

        self.filter_from = moment().subtract(29, 'days').format('YYYY-MM-DD');
        self.filter_to = moment().format('YYYY-MM-DD');
    }

    /**
     * Hide Alert Message
     */
    hideAlertMessage() {
        $('#green_message').hide();
        $('#green_message_text').text('');
        $('#red_message').hide();
        $('#red_message_text').text('');
        $('#yellow_message').hide();
        $('#yellow_message_text').text('');
    }

    /**
     * Show Spinner
     */
    show_spinner() {
        $("#overlay").css("display", "flex");
    }

    /**
     * Hide Spinner
     */
    hide_spinner() {
        $("#overlay").css("display", "none");
    }

    /**
     * Error Handler
     */
    errorHandler() {
        $('#red_message').show();
        $('#red_message_text').text("Sending data error, please try again later.");

        this.hide_spinner();
    }

    /**
     * Success Handler
     *
     * @param response
     */
    successHandler(response) {
        var self = this;

        if (typeof (response.status) !== 'undefined' && response.status === false) {
            $('#red_message').show();
            $('#red_message_text').text(response.message);
        } else {
            if (typeof (response.data) !== 'undefined') {
                let data = response.data;

                $.each(data, function (index, v) {
                    if (typeof (self.reportItems[v.id]) === "undefined") {
                        self.reportItems[v.id] = v;

                        let time = self.convertMinutesToHours(v.time_minute) + (v.employer === null ? " / " + self.convertMinutesToHours(v.time_minute + v.sum_child_time_minute) : "");

                        self.mainTable.row.add([
                            v.id,
                            v.name,
                            (v.is_valid_email ? v.email : "<p class='text-red'>" + v.email + "</p>"),
                            self.renderHierarchy(v.hierarchy),
                            time,
                            self.renderNotWorkedOutPopover(v.id, v.not_finalized)
                        ]).draw(false);
                    }
                });


                $('[data-toggle="popover"]').popover({
                    placement: 'top',
                    trigger: 'hover',
                    html: true
                });

                $('[data-toggle="user_info"]').popover({
                    placement: 'bottom',
                    html: true
                });


                $("#report_1 > tbody > tr, #report_1 > tbody")
                    .mouseenter(function () {
                        let el = $('#popover_user_info');
                        var id;
                        if ($(this).attr('role') === "row") {
                            id = $(this).find(".sorting_1").html()
                        }

                        if ($(this).parent().attr('role') === "row") {
                            id = $(this).parent().find(".sorting_1").html()
                        }

                        if (typeof (id) !== 'undefined') {
                            el.attr('data-content', self.getUserInfo(id));
                            el.popover('show');
                        }
                    })
                    .mouseleave(function () {
                        var id;
                        if ($(this).attr('role') === "row") {
                            id = $(this).find(".sorting_1").html()
                        }

                        if ($(this).parent().attr('role') === "row") {
                            id = $(this).parent().find(".sorting_1").html()
                        }

                        if (!id) {
                            $('#popover_user_info').popover('hide');
                        }

                    });
            }
        }

        this.hide_spinner();
    }

    /**
     * Get User Info
     *
     * @param id
     * @returns {string}
     */
    getUserInfo(id) {
        var html = "";
        if (typeof (this.reportItems[id]) !== "undefined") {
            let v = this.reportItems[id];
            html = '<div class="box box-widget widget-user-2">' +
                '            <div class="widget-user-header bg-aqua-active">' +
                '              <div class="widget-user-image">' +
                '                <img class="img-circle" src="assets/img/placeholder-man-360x360.png" alt="User Avatar">' +
                '              </div>' +
                '              <h3 class="widget-user-username">' + v['name'] + '</h3>' +
                '              <h5 class="widget-user-desc">' + v['email'] + '</h5>' +
                '            </div>' +
                '            <div class="box-footer no-padding">' +
                '              <dl>' +
                '                <dt>Информация</dt>' +
                '                <dd>' + v['info'] + '</dd>' +
                '              </dl>' +
                '            </div>' +
                '          </div>';
        }
        return html;
    }

    /**
     * Convert Minutes To Hours
     *
     * @param totalMinutes
     * @returns {string}
     */
    convertMinutesToHours(totalMinutes) {
        var hours = Math.floor(totalMinutes / 60);
        var minutes = totalMinutes % 60;
        return hours + ":" + (minutes !== 0 ? minutes : "00");
    }

    /**
     * Render Not Worked Out Popover
     *
     * @param id
     * @param data
     * @returns {string}
     */
    renderNotWorkedOutPopover(id, data) {
        var html = "";

        if (typeof (data) !== "undefined" && Object.keys(data).length !== 0) {
            html += '<button type="button" class="btn btn-block text-yellow" data-user_id="' + id + '" data-toggle="popover" title="Недоработка"  data-content="' + this.renderNotWorkedOut(data) + '">Недоработка</button>';
        }

        return html;
    }

    /**
     * Render Not Worked Out
     *
     * @param data
     * @returns {string}
     */
    renderNotWorkedOut(data) {
        var html = "";

        if (typeof (data) !== "undefined" && Object.keys(data).length !== 0) {
            html += "<div>";
            $.each(data, function (index, v) {
                html += '<p class=\'text-yellow\'>' + v['date'] + ' - ' + v['time'] + '</p>';
            });
            html += '</div>'
        }

        return html;
    }

    /**
     * Render Hierarchy
     *
     * @param data
     * @returns {string}
     */
    renderHierarchy(data) {
        var html = "";

        if (typeof (data) !== "undefined" && Object.keys(data).length !== 0) {
            data = data.reverse();
            html += '<ul class="timeline">';
            $.each(data, function (index, v) {
                html += '<li><i class="fa fa-user bg-gray"></i><div class="timeline-item-report"><a href="#"><p class="text-muted">' + v.name + '</a></p></div></li>';
            });
            html += '<li><i class="fa fa-user bg-aqua"></i></li></ul>'
        }

        return html;
    }

    /**
     * Create Report Request
     *
     * @returns {boolean}
     */
    reportRequest() {
        var self = this;

        self.hideAlertMessage();

        if (self.filter_from === "" && self.filter_to === "") {
            $('#red_message').show();
            $('#red_message_text').text("Please select a date.");
            return false;
        }

        //reset DataTable
        $('#report_1').DataTable().clear();
        $('#report_1').find('tr:gt(0)').remove();
        self.reportItems = {};

        self.show_spinner();
        $('#popover_user_info').popover('hide');

        $.ajax({
            url: "/getreport.php",
            dataType: 'json',
            type: 'post',
            data: {from: self.filter_from, to: self.filter_to},
            success: self.successHandler.bind(self),
            error: self.errorHandler
        }).done(function () {
            self.hide_spinner();
        });
    }
}