<?php

namespace app\core;

/**
 * Class Logging
 *
 * @package app\core
 */
class Logging
{
    const LOG_FILE_DIR = "logs";

    // types of typical log levels
    const CRITICAL = "CRITICAL";
    const ERROR = "ERROR";
    const WARNING = "WARNING";
    const NOTICE = "NOTICE";
    const MESSAGE = "MESSAGE";

    // types of log messages
    const CONFIGURATION_DIR_NOT_FOUND = "CONFIGURATION_DIR_NOT_FOUND %s";
    const CONFIGURATION_MODULES_NOT_FOUND = "CONFIGURATION_MODULES_NOT_FOUND %s";
    const CONFIGURATION_SYSTEM_MODULE_NOT_FOUND = "CONFIGURATION_SYSTEM_MODULE_NOT_FOUND %s";

    const MODULE_NOT_FOUND = "MODULE_NOT_FOUND %s";
    const MODEL_NOT_FOUND = "MODEL_NOT_FOUND %s";
    const VIEW_NOT_FOUND = "VIEW_NOT_FOUND %s";
    const CONTROLLER_NOT_FOUND = "CONTROLLER_NOT_FOUND %s";
    const HELPER_NOT_FOUND = "HELPER_NOT_FOUND %s";
    const CONFIGURATION_NOT_FOUND = "CONFIGURATION_NOT_FOUND %s";
    const DATASOURCE_MODEL_NOT_FOUND = "DATASOURCE_MODEL_NOT_FOUND %s";

    /**
     * Prepare log file before output.
     * Generate String
     *
     * @param String $level
     * @param String $message
     * @param String|null $_
     * @return string
     */
    public static function format(String $level, String $message, String $_ = null)
    {
        return $level . " ::: " . sprintf($message, $_);
    }

    /**
     * Log Message
     *
     * @param String $level
     * @param String $logMessage
     */
    public static function log(String $level, String $logMessage)
    {
        // log file name
        $logFileName = Core::getDocumentRoot() . DIRECTORY_SEPARATOR . self::LOG_FILE_DIR . DIRECTORY_SEPARATOR;
        $logFileName .= "errors_" . date('m-d-Y') . ".log";

        // save log to the file
        file_put_contents($logFileName, $level . " ::: " . $logMessage . PHP_EOL, FILE_APPEND);
    }

}