<?php

namespace app\core;

use app\datasource\DatasourceModel;
use app\models\User;
use app\core\Logging;

use Exception;

/**
 * Class Core
 *
 * @package app\core
 */
class Core
{

    // class private variables
    private $documentRoot = '..';

    /**
     * Core constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {

    }

    public static function getDBConfiguration(): array
    {
        return include('settings/db_config.php');
    }

    /**
     * Get Application Document Root
     *
     * @return String
     */
    public function getDocumentRoot(): String
    {
        return $this->documentRoot;
    }

    /**
     * Get User Model class
     *
     * @return User
     * @throws Exception
     */
    public function getUserModel(): User
    {
        $user = new User();
        return $user;
    }

}