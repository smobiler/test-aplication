<?php

namespace app\models;

use app\core\Core;
use app\datasource\database\user\UserModel;

use Exception;

/**
 * Class User / Model
 *
 * @package app\models\user
 */
class User implements \app\models\Model
{

    private $datasourceModel = null;

    /**
     * Model constructor.
     */
    public function __construct()
    {
        return $this->getDatasourceModel();
    }

    /**
     * Get Datasource Model
     *
     * @return UserModel|null
     */
    public function getDatasourceModel()
    {
        if ($this->datasourceModel === null) {
            $this->datasourceModel = new UserModel(Core::getDBConfiguration());
        }
        return $this->datasourceModel;
    }

    /**
     * Load all users from Database
     *
     * @return array
     * @throws Exception
     */
    public function loadUsers($from, $to): array
    {
        $resourseModel = $this->getDatasourceModel();

        $userList = $resourseModel->loadUsers($from, $to);

        $users = [];
        $usersGrouped = [];

        $parentUsersStats = [];


        foreach ($userList as $user) {
            $time = date_parse($user['time']);
            $user['time_minute'] = ($time['hour'] * 60) + $time['minute'];
            $user['not_finalized'] = [];

            if (empty($users[$user['id']])) {
                $users[$user['id']] = $user;
                $users[$user['id']]['is_valid_email'] = (bool)(filter_var($user['email'], FILTER_VALIDATE_EMAIL));
            } else {
                $users[$user['id']]['time_minute'] += $user['time_minute'];
            }

            if ($user['time_minute'] < 480) {
                $users[$user['id']]['not_finalized'][] = ['date' => $user['date'], 'time' => $user['time']];
            }
        }

        foreach ($users as $key => $user) {
            if (!empty($user['employer'])) {
                $usersGrouped[$user['employer']][] = $user;
            } else {
                $users[$user['id']]['hierarchy'] = [];
            }
        }

        foreach ($users as $key => $user) {
            if (!empty($user['employer'])) {
                $this->getUsersHierarchyParent($users, $user['employer'], $user['id']);
            }
            $this->getUsersHierarchyChild($parentUsersStats, $usersGrouped, $user['id'], $user['id']);
        }

        foreach ($parentUsersStats as $id => $parentUser) {
            if (!empty($users[$id])) {
                $users[$id]['sum_child_time_minute'] = $parentUser['sum_child_time_minute'];
            }
        }

        if (sizeof($users) == 0) return [];

        return $users;
    }

    /**
     * Get Users Hierarchy Parent
     *
     * @param $users
     * @param $employerId
     * @param $id
     */
    private function getUsersHierarchyParent(&$users, $employerId, $id)
    {
        if (is_array($users) && count($users[$employerId]) > 0) {
            foreach ($users as $user) {
                if ($user['id'] == $employerId && $id != $user['id']) {
                    $users[$id]['hierarchy'][] = ['id' => $user['id'], 'name' => $user['name']];
                    if (!empty($user['employer'])) {
                        $this->getUsersHierarchyParent($users, $user['employer'], $id);
                    }
                }
            }
        }
    }

    /**
     * Get Users Hierarchy Child
     *
     * @param $parentUsersStats
     * @param $users
     * @param $id
     * @param $parentId
     */
    private function getUsersHierarchyChild(&$parentUsersStats, $users, $id, $parentId)
    {
        if (is_array($users) and count($users[$id]) > 0) {
            foreach ($users[$id] as $user) {
                $parentUsersStats[$parentId]['childIds'][] = $user['id'];
                $parentUsersStats[$parentId]['sum_child_time_minute'] += $user['time_minute'];
                $this->getUsersHierarchyChild($parentUsersStats, $users, $user['id'], $parentId);
            }
        }
    }


}