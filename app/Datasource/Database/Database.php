<?php

namespace app\datasource\database;

use app\core\Logging;
use Exception;

/**
 * Class Database
 *
 * Contains common methods to works with Database
 *
 * @package app\datasource\database
 */
class Database
{
    private $connect;
    private $dataSourceConfiguration;

    /**
     * Database constructor.
     *
     * @param array $dataSourceConfiguration
     */
    public function __construct(array $dataSourceConfiguration)
    {
        // set default
        $this->connect = null;
        // set parameters
        $this->dataSourceConfiguration = $dataSourceConfiguration;
    }

    /**
     * Get Database Connection
     *
     * @return mixed $connect
     */
    public function connect()
    {
        // if connect created
        if ($this->connect != null) return $this->connect;

        // create new connect
        return $this->getConnections();
    }

    /**
     * Close DB Connection
     */
    public function close()
    {
        if ($this->connect != null) mysqli_close($this->connect);

        $this->connect = null;
    }

    /**
     * Select query from Database
     *
     * @param String $sql
     * @param array $argv
     * @return array
     */
    public function select(String $sql, array $argv): array
    {
        try {
            // connect to db
            $link = $this->connect();
            if ($link == null) throw new Exception("Can't connect to the Database");

            // prepare sql query
            if (sizeof($argv) == 0) {
                $query = $sql;
            } else {
                $query = vsprintf($sql, $argv);
                if ($query == false) throw new Exception("Can't create SQL Query");
            }

            // run query
            $result = mysqli_query($link, $query);
            if ($result === false) {
                $error = mysqli_error($link);
                throw new Exception("Can't exec SQL Query; " . $error);
            }

            if ($result === true) {
                return [];
            }

            //get associative array
            $data = mysqli_fetch_all($result, MYSQLI_ASSOC);
            // clear result
            mysqli_free_result($result);

            // close connection
            $this->close();

            // return data
            return $data;

        } catch (Exception $ex) {
            Logging::log(Logging::ERROR, $ex->getMessage());
            $this->close();
        }

        return [];
    }

    /**
     * Escapes special characters in a string for use in an SQL statement
     *
     * @param string $value
     * @return string
     */
    public function escape(string $value): string
    {
        $link = $this->connect();
        return $link->real_escape_string($value);
    }


    /**
     * Get Connection to the DB
     *
     * @return mixed $connect
     */
    private function getConnections()
    {
        // check if configuration file set
        if (!isset($this->dataSourceConfiguration)) return null;

        // get magento configuration
        $sourceConfiguration = $this->dataSourceConfiguration;
        // check if configuration set
        if (sizeof($sourceConfiguration) == 0) return null;

        // check params
        if (!isset($sourceConfiguration['host']) || !isset($sourceConfiguration['port']) || !isset($sourceConfiguration['user'])
            || !isset($sourceConfiguration['password']) || !isset($sourceConfiguration['database'])) return null;

        // set up new connection
        $link = mysqli_connect($sourceConfiguration['host'], $sourceConfiguration['user'], $sourceConfiguration['password'],
            $sourceConfiguration['database'], $sourceConfiguration['port']);

        // if can't set up
        if (!$link) return null;

        // connection ok
        $this->connect = $link;

        // return connection
        return $this->connect;
    }

}