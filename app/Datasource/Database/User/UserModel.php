<?php

namespace app\datasource\database\user;

use app\datasource\database\Database;
use app\datasource\DatasourceModel;

/**
 * Class User
 *
 * @package app\datasource\database\user
 */
class UserModel implements DatasourceModel
{
    private $database;

    /**
     * UserModel constructor.
     *
     * @param array $dataSourceConfiguration
     */
    public function __construct(array $dataSourceConfiguration)
    {
        $this->database = new Database($dataSourceConfiguration);
    }

    /**
     * Load Users From Database
     *
     * @return array
     */
    public function loadUsers($from, $to): array
    {

        $where = [];

        if (!empty($from)) {
            $where[] = $this->database->escape($from);
        }

        if (!empty($to)) {
            $where[] = $this->database->escape($to);
        }

        return $this->database->select("SELECT" . " e.*, t.time, DATE_FORMAT(t.date, '%%Y-%%m-%%d') as date FROM employees as e LEFT JOIN timesheet as t ON t.employee_id = e.id " .
            (!empty($where) ? "WHERE " . (!empty($from) ? " t.date >= '%s' " : "") : "") . (!empty($from) && !empty($to) ? " AND " : "") . (!empty($to) ? " t.date <= '%s' " : "")
            , $where);
    }

}