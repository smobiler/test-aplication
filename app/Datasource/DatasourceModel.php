<?php

namespace app\datasource;

/**
 * Interface DatasourceModel
 *
 * @package app\datasource
 */
interface DatasourceModel
{

    /**
     * DatasourceModel constructor.
     *
     * @param array $dataSourceConfiguration
     */
    public function __construct(array $dataSourceConfiguration);

}