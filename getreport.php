<?php
require __DIR__ . '/vendor/autoload.php';

use app\core\Core;
use app\core\Logging;

$result = [];
$data = [];
$result['status'] = false;
$result['message'] = "";

try {

    $from = htmlspecialchars($_POST["from"]);
    $to = htmlspecialchars($_POST["to"]);

    if (empty($from) && empty($to)) {
        $result['message'] = "Please select a date.";
        return json_encode($result);
    }

    $core = new Core();
    $user = $core->getUserModel();

    $users = $user->loadUsers($from, $to);
    $result['data'] = $users;
    $result['status'] = true;

    header('Content-Type: application/json');
    echo json_encode($result);
    exit();

} catch (\Throwable $ex) {
    // log error message
    Logging::log(Logging::ERROR, $ex->getMessage());
    var_dump($ex->getMessage());
}
